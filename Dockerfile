FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/hello_world.jar test_hello_world.jar
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /test_hello_world.jar" ]
