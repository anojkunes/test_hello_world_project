/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Anoj
 */
@RestController
public class TestController {
    
    @Value("${user.test.detail.firstname}")
    private String firstName;
    @Value("${user.test.detail.lastname}")
    private String lastname;
    @Value("${user.test.detail.status}")
    private String status;

    @RequestMapping("/")
    public String index() {
        return "Hello World";
    }
    
    @RequestMapping("/test")
    public Map<String, String> test() {
        Map<String, String> map = new HashMap<>();
        map.put("firstname", firstName);
        map.put("lastname", lastname);
        map.put("status", status);
        return map;
    }
}
